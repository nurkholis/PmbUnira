webpackJsonp([0],{

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__beasiswa_beasiswa__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__biaya_biaya__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__fakultas_fakultas__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__jalur_jalur__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_kab_search_kab__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_api_unira_api_unira__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_date_picker__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HomePage = (function () {
    function HomePage(platform, locationAccuracy, alertCtrl, geolocation, datePicker, navParams, events, http, toastCtrl, loadingCtrl, ApiUnira, navCtrl) {
        this.platform = platform;
        this.locationAccuracy = locationAccuracy;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.datePicker = datePicker;
        this.navParams = navParams;
        this.events = events;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.ApiUnira = ApiUnira;
        this.navCtrl = navCtrl;
        this.pmb = "beranda";
        this.kode_wilayah = '';
        this.prodiList = [];
        this.sekolahList = [];
        this.kodeList = [];
        this.kodePengumuman = '';
        this.status = 'defaut';
        this.daftar = {
            'prodi': '22',
            'prodi2': '21',
            'masuk': '1',
            'kelas': '1',
            'nama': 'Dummy',
            'agama': '1',
            'ktp': '1234567890',
            'jeniskelamin': 'L',
            'lahirkota': '',
            'lahirkotanama': '',
            'lahirtanggal': '20/07/1996',
            'alamat': '',
            'kota': '',
            'kotanama': '',
            'kodepos': '',
            'telepon': '085233889578',
            'email': 'coab@coba.coba',
            'smakota': '',
            'smakotanama': '',
            'smanama': '',
            'smatahun': '2016',
            'smajurusan': 'IPS',
            'smanilai': '90',
            'bentuksekolah': '',
            'time': ''
        };
        this.lulus = {
            nama: '',
            kode: '',
            status: false,
            diterima: null,
        };
        this.loadstatus = false;
        this.closesHandle();
        //localStorage.clear();
        // membuat data kode
        if (localStorage.getItem('kode') == null) {
            console.log('membuat kode');
            var a = {
                'data': []
            };
            localStorage.setItem('kode', JSON.stringify(a));
        }
    }
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.navClicked = function () {
        var _this = this;
        console.log(this.pmb);
        if (this.pmb == 'daftar') {
            if (this.loadstatus == false) {
                this.activatedGPS();
                this.loadstatus = true;
                var loader_1 = this.loadingCtrl.create({
                    content: "Please wait..."
                });
                loader_1.present().then(function () {
                    //loader.dismiss();
                    //mengambil data prodi
                    _this.getProdi().then(function (result) {
                        _this.prodiList = result['data'];
                        loader_1.dismiss();
                    }).catch(function (error) {
                        _this.gagalKoneksi();
                        loader_1.dismiss();
                    });
                });
            }
            var now = (new Date().getTime()) / 1000;
            this.daftar.time = now.toFixed(0);
            console.log(this.daftar.time);
        }
        if (this.pmb == 'kelulusan') {
            //this.kelulusan();
            this.showKode();
        }
    };
    HomePage.prototype.closesHandle = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.platform.registerBackButtonAction(function () {
                var alertClose = _this.alertCtrl.create({
                    title: 'Keluar dari aplikasi ?',
                    buttons: [
                        {
                            text: 'Batal', role: 'cancel', handler: function (kelas) {
                            }
                        },
                        {
                            text: 'Keluar', handler: function (data) {
                                _this.platform.exitApp();
                            }
                        }
                    ]
                });
                alertClose.present();
            });
        });
    };
    HomePage.prototype.gagalKoneksi = function (s) {
        if (s === void 0) { s = null; }
        if (s == null) {
            var toast = this.toastCtrl.create({
                message: 'Koneksi gagal',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
        else {
            var toast = this.toastCtrl.create({
                message: s,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    // kode beranda
    HomePage.prototype.pushPage = function (page) {
        if (page == 'fakultas') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__fakultas_fakultas__["a" /* FakultasPage */]);
        }
        if (page == 'jalur') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__jalur_jalur__["a" /* JalurPage */]);
        }
        if (page == 'biaya') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__biaya_biaya__["a" /* BiayaPage */]);
        }
        if (page == 'beasiswa') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__beasiswa_beasiswa__["a" /* BeasiswaPage */]);
        }
    };
    // kahir kode beranda
    //kode daftar
    HomePage.prototype.activatedGPS = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                    _this.loadLocation();
                    _this.status = 'actived xx';
                }, function (error) {
                    _this.status = 'disactived';
                });
            }
            else {
                _this.status = 'not can request';
            }
        });
    };
    HomePage.prototype.loadLocation = function () {
        var _this = this;
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        var watch = this.geolocation.watchPosition(options);
        watch.subscribe(function (data) {
            if (data.coords !== undefined) {
                _this.status = data.coords.latitude;
                _this.latitude = data.coords.latitude;
                _this.longitude = data.coords.longitude;
                console.log(_this.latitude, _this.longitude);
                // harus registes key
                _this.getAddress(_this.latitude, _this.longitude).then(function (result) {
                    _this.daftar.alamat = result['results'][0]['formatted_address'];
                    var data = result['results'][0]['address_components'];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i]['types'][0] == 'postal_code') {
                            console.log(data[i]['long_name']);
                            _this.daftar.kodepos = data[i]['long_name'];
                        }
                    }
                }).catch(function (reject) {
                    console.log('gagal memuat alamat');
                });
            }
            else {
                console.log('Error lokasi');
                _this.status = 'Error lokasi';
            }
        });
    };
    HomePage.prototype.getDate = function () {
        var _this = this;
        console.log('getdate');
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(function (date) {
            console.log('Got date: ', date);
            var today = date;
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var jadi = dd + '/' + mm + '/' + yyyy;
            _this.daftar.lahirtanggal = jadi;
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
            _this.status = err;
        }
        // date => console.log('Got date: ', date),
        // err => console.log('Error occurred while getting date: ', err)
        );
    };
    HomePage.prototype.getProdi = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.ApiUnira.get('prodi').then(function (result) {
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    HomePage.prototype.getAddress = function (lat, long) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long).subscribe(function (data) {
                resolve(data);
                console.log('get addr');
            }, function (error) {
                reject(error);
            });
        });
    };
    HomePage.prototype.getKabupaten = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.ApiUnira.get('wilayah', { 'limit': '1000' }).then(function (result) {
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    HomePage.prototype.search = function (to) {
        var _this = this;
        if (to == 'asal kota' || to == 'lahir kota' || to == 'sma kota') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__search_kab_search_kab__["a" /* SearchKabPage */], { 'mode': to });
            this.events.subscribe('search:created', function (key, name) {
                if (to == 'asal kota') {
                    _this.daftar.kota = key;
                    _this.daftar.kotanama = name;
                }
                if (to == 'lahir kota') {
                    _this.daftar.lahirkota = key;
                    _this.daftar.lahirkotanama = name;
                }
                if (to == 'sma kota') {
                    _this.daftar.smakota = key;
                    _this.daftar.smakotanama = name;
                }
                _this.events.unsubscribe('search:created', null);
            });
        }
        if (to == 'sma nama') {
            var param = {
                'mode': to,
                'bentuk': this.daftar.bentuksekolah,
                'kode_wilayah': this.daftar.smakota
            };
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__search_kab_search_kab__["a" /* SearchKabPage */], param);
            this.events.subscribe('search:created', function (key, name) {
                _this.daftar.smanama = name;
                _this.events.unsubscribe('search:created', null);
            });
        }
    };
    HomePage.prototype.bentuksekolahChanged = function () {
        this.daftar.smanama = '';
    };
    HomePage.prototype.smaKotaChanged = function () {
        this.daftar.bentuksekolah = '';
        this.daftar.smanama = '';
    };
    HomePage.prototype.pmbDaftar = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.ApiUnira.post('pmbpendaftar', _this.daftar).then(function (result) {
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    HomePage.prototype.btnDaftarClicked = function () {
        var _this = this;
        console.log(this.daftar);
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present().then(function () {
            _this.pmbDaftar().then(function (result) {
                var nomor = result['data']['attributes']['nomor'];
                var nama = result['data']['attributes']['nama'];
                var alert = _this.alertCtrl.create({
                    title: 'Berhasil Mendaftar',
                    subTitle: 'Anda terdaftar dengan Kode ' + nomor + ' dan Nama ' + nama + ". Bukti pendaftaran sudah dikirim ke e-mail anda",
                    buttons: ['Ok']
                });
                loader.dismiss();
                alert.present();
                //menambah data kode
                var b = localStorage.getItem('kode');
                b = JSON.parse(b);
                b['data'].push({ 'nama': nama, 'kode': nomor });
                localStorage.setItem('kode', JSON.stringify(b));
                console.log(JSON.parse(localStorage.getItem('kode')));
                _this.showKode();
                //mengirim email pada pendaftar
                _this.http.get('http://api1.unira.ac.id/v1/pmb/mail/' + nomor).subscribe(function (data) {
                }, function (error) {
                });
            }).catch(function (error) {
                console.log(error);
                console.log(error['error']['errors'][0]['detail']);
                var detail = error['error']['errors'][0]['detail'];
                var alert = _this.alertCtrl.create({
                    title: 'Gagal Mendaftar',
                    subTitle: detail,
                    buttons: ['Ok']
                });
                alert.present();
                loader.dismiss();
            });
        });
    };
    HomePage.prototype.btnDaftarReset = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Reset',
            message: 'Semua data yang ada dalam form akan di hapus',
            buttons: [
                {
                    text: 'Batal',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Oke',
                    handler: function () {
                        _this.resetForm();
                    }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.resetForm = function () {
        this.daftar.prodi = '';
        this.daftar.prodi2 = '';
        this.daftar.masuk = '';
        this.daftar.kelas = '';
        this.daftar.nama = '';
        this.daftar.agama = '';
        this.daftar.ktp = '';
        this.daftar.jeniskelamin = '';
        this.daftar.lahirkota = '';
        this.daftar.lahirkotanama = '';
        this.daftar.lahirtanggal = '';
        this.daftar.alamat = '';
        this.daftar.kota = '';
        this.daftar.kotanama = '';
        this.daftar.kodepos = '';
        this.daftar.telepon = '';
        this.daftar.email = '';
        this.daftar.smakota = '';
        this.daftar.smakotanama = '';
        this.daftar.bentuksekolah = '';
        this.daftar.smanama = '';
        this.daftar.smatahun = '';
        this.daftar.smajurusan = '';
        this.daftar.smanilai = '';
    };
    //akhir kode daftar
    //kode kelulusan
    HomePage.prototype.showKode = function () {
        var kode = JSON.parse(localStorage.getItem('kode'));
        this.kodeList = kode['data'];
        this.kodePengumuman = this.kodeList[this.kodeList.length - 1]['kode'];
    };
    HomePage.prototype.getPengumuman = function (a) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present().then(function () {
            _this.http.get('http://api1.unira.ac.id/v1/pmb/pendaftar/' + a + '?field=nama,diterima').subscribe(function (data) {
                _this.lulus.kode = data['data']['id'];
                _this.lulus.nama = data['data']['attributes']['nama'];
                _this.lulus.diterima = data['data']['attributes']['diterima'];
                if (_this.lulus.diterima != null) {
                    _this.lulus.status = true;
                }
                else {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Belum ada pengumuman !',
                        subTitle: 'Untuk informasi lebih jelas silahkan kunjungi website UNIRA atau hubungi admin PMB',
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
                console.log(_this.lulus);
                loader.dismiss();
            }, function (error) {
                loader.dismiss();
                _this.gagalKoneksi();
            });
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar no-border-bottom color="color1">\n    <ion-title>\n      PMB UNIRA\n    </ion-title>\n  </ion-navbar>\n  <ion-toolbar color="color1">\n    <ion-segment [(ngModel)]="pmb">\n      <ion-segment-button value="beranda" (click)="navClicked()" class="putih">\n        Beranda\n      </ion-segment-button>\n      <ion-segment-button value="daftar" (click)="navClicked()">\n        Daftar\n      </ion-segment-button>\n      <ion-segment-button value="kelulusan" (click)="navClicked()">\n        Kelulusan\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div [ngSwitch]="pmb">\n\n    <!-- content bernada -->      \n    <ion-list *ngSwitchCase="\'beranda\'">\n      <ion-grid>\n        <ion-row>\n            <ion-col col-6>\n              <ion-card (click)="pushPage(\'fakultas\')">\n                <ion-card-content class="no-padding">\n                  <img src="assets/imgs/fakultas.png" alt="">\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n            <ion-col col-6>\n              <ion-card (click)="pushPage(\'jalur\')">\n                <ion-card-content class="no-padding">\n                  <img src="assets/imgs/jalur.png" alt="">\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6>\n              <ion-card (click)="pushPage(\'biaya\')">\n                <ion-card-content class="no-padding">\n                  <img src="assets/imgs/biaya.png" alt="">\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n            <ion-col col-6>\n              <ion-card (click)="pushPage(\'beasiswa\')">                \n                <ion-card-content class="no-padding">\n                  <img src="assets/imgs/beasiswa.png" alt="">\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-list>\n\n    <!-- content daftar -->      \n    <ion-list *ngSwitchCase="\'daftar\'">\n      <ion-card>\n        <ion-card-header>\n          <!-- {{status}} -->\n        </ion-card-header>\n        <ion-card-content >\n          \n          <div class="pilihan-prodi">\n            <ion-item-divider color="light" text-center>Pilihan Prodi</ion-item-divider>\n            <!-- prodi1 -->\n            <br>\n            <ion-item>\n              <ion-label>Prodi Pilihan 1</ion-label>\n              <ion-select [(ngModel)]="daftar.prodi">\n                <ion-option *ngFor="let data of prodiList" value="{{data.id}}">\n                {{data.attributes.nama}}\n                </ion-option>\n              </ion-select>\n            </ion-item>\n            <!-- prodi 2 -->\n            <br>\n            <ion-item>\n              <ion-label>Prodi Pilihan 2</ion-label>\n              <ion-select [(ngModel)]="daftar.prodi2" disabled="{{daftar.prodi == \'\'}}">\n                <ion-option *ngFor="let data of prodiList" value="{{data.id}}">\n                {{data.attributes.nama}}\n                </ion-option>\n              </ion-select>\n            </ion-item>\n            <!-- masuk -->\n            <ion-list radio-group [(ngModel)]="daftar.masuk">\n              <ion-grid>\n                <ion-row>\n                  <ion-col col-4>\n                    <ion-label>Masuk</ion-label>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="1" checked></ion-radio>\n                      <ion-label>Baru</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="2"></ion-radio>\n                      <ion-label>Transfer</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-list>\n            <!-- kelas -->\n            <ion-list radio-group [(ngModel)]="daftar.kelas">\n              <ion-grid>\n                <ion-row>\n                  <ion-col col-4>\n                    <ion-label>Kelas</ion-label>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="1" checked></ion-radio>\n                      <ion-label>Pagi</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="2"></ion-radio>\n                      <ion-label>Sore</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-list>\n          </div>\n\n          <div class="identitas-diri">\n            <ion-item-divider color="light" text-center>Identitas Diri</ion-item-divider>\n            <!-- nama -->\n            <ion-item>\n              <ion-label floating>Nama Lengkap</ion-label>\n              <ion-input type="text" [(ngModel)]="daftar.nama"></ion-input>\n            </ion-item>\n            <!-- agama -->\n            <br>\n            <ion-item>\n              <ion-label>Agama</ion-label>\n              <ion-select [(ngModel)]="daftar.agama">\n                <ion-option value="1">Islam</ion-option>\n                <ion-option value="2">Kristen</ion-option>\n                <ion-option value="3">Khatolik</ion-option>\n                <ion-option value="4">Hindu</ion-option>\n                <ion-option value="5">Budha</ion-option>\n                <ion-option value="6">Konghuchu</ion-option>\n                <ion-option value="7">Lainnya</ion-option>\n              </ion-select>\n            </ion-item>\n            <!-- ktp -->\n            <ion-item>\n              <ion-label floating>No KTP</ion-label>\n              <ion-input type="number" [(ngModel)]="daftar.ktp"></ion-input>\n            </ion-item>\n            <!-- jeniskelamin -->\n            <ion-list radio-group [(ngModel)]="daftar.jeniskelamin">\n              <ion-grid>\n                <ion-row>\n                  <ion-col col-4>\n                    <ion-label>Gender</ion-label>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="L" checked></ion-radio>\n                      <ion-label>Pria</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col col-4>\n                    <ion-item>\n                      <ion-radio value="P"></ion-radio>\n                      <ion-label>Wanita</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-list>\n            <!-- lahirkota -->\n            <br>\n            <div>\n              <div class="tabir" (click)="search(\'lahir kota\')"></div>\n              <ion-item >\n                <ion-label>Lahir Kota</ion-label>\n                <ion-select [(ngModel)]="daftar.lahirkota">\n                  <ion-option selected value="{{daftar.lahirkota}}">{{daftar.lahirkotanama}}</ion-option>\n                </ion-select>\n              </ion-item>\n            </div>\n            <!-- lahirtanggal -->\n            <br>\n            <ion-item>\n              <ion-label floating>Tgl Lahir</ion-label>\n              <ion-input type="text" [(ngModel)]="daftar.lahirtanggal" disabled="true"></ion-input>\n              <!-- <button clear item-right>Forgot</button> -->\n              <button style="padding: 18px 15px; font-size: 15px" ion-button item-right color="color1" (click)="getDate()" ><ion-icon name="calendar"></ion-icon></button>\n            </ion-item>\n            <!-- alamat -->\n            <ion-item>\n              <ion-label floating>Alamat</ion-label>\n              <ion-input type="text" [(ngModel)]="daftar.alamat"></ion-input>\n            </ion-item>\n            <!-- kota -->\n            <br>\n            <div>\n              <div class="tabir" (click)="search(\'asal kota\')"></div>\n              <ion-item >\n                <ion-label>Kota</ion-label>\n                <ion-select [(ngModel)]="daftar.kota">\n                  <ion-option selected value="{{daftar.kota}}">{{daftar.kotanama}}</ion-option>\n                </ion-select>\n              </ion-item>\n            </div>\n            <!-- kodepos -->\n            <ion-item>\n              <ion-label floating>Kode Pos</ion-label>\n              <ion-input type="number" [(ngModel)]="daftar.kodepos"></ion-input>\n            </ion-item>\n            <!-- telepon -->\n            <ion-item>\n              <ion-label floating>No Tlp</ion-label>\n              <ion-input type="number" [(ngModel)]="daftar.telepon"></ion-input>\n            </ion-item>\n            <!-- email -->\n            <ion-item>\n              <ion-label floating>Email</ion-label>\n              <ion-input type="email" [(ngModel)]="daftar.email"></ion-input>\n            </ion-item>\n\n          </div>\n\n          <div class="sekolah-asal">\n            <ion-item-divider color="light" text-center>Sekolah / Universitas Asal</ion-item-divider>\n            <!-- smakota -->\n            <br>\n            <div>\n              <div class="tabir" (click)="search(\'sma kota\')"></div>\n              <ion-item >\n                <ion-label>Kota SMA/MA/PT</ion-label>\n                <ion-select [(ngModel)]="daftar.smakota" (ngModelChange)=\'smaKotaChanged()\'>\n                  <ion-option selected value="{{daftar.smakota}}">{{daftar.smakotanama}}</ion-option>\n                </ion-select>\n              </ion-item>\n            </div>\n            <!-- bentuksekolah -->\n            <br>\n            <ion-item>\n              <ion-label>Bentuk Sekolah</ion-label>\n              <ion-select [(ngModel)]="daftar.bentuksekolah" (ngModelChange)=\'bentuksekolahChanged()\' disabled="{{daftar.smakota == \'\'}}">\n                <ion-option value="sma">SMA</ion-option>\n                <ion-option value="smk">SMK</ion-option>\n                <ion-option value="ma">MA</ion-option>\n                <ion-option value="pt">Perguruan Tinggi</ion-option>\n              </ion-select>\n            </ion-item>\n            <!-- smanama -->\n            <br>\n            <div>\n              <div *ngIf="daftar.bentuksekolah != \'pt\'">\n                <div class="tabir" (click)="search(\'sma nama\')"></div>\n                <ion-item >\n                  <ion-label>SMA/MA/PT Nama</ion-label>\n                  <ion-select [(ngModel)]="daftar.smanama" disabled="{{daftar.bentuksekolah == \'\'}}">\n                    <ion-option selected value="{{daftar.smanama}}">{{daftar.smanama}}</ion-option>\n                  </ion-select>\n                </ion-item>\n              </div>\n              <div *ngIf="daftar.bentuksekolah == \'pt\'">\n                <ion-item>\n                  <ion-label floating>Nama PT</ion-label>\n                  <ion-input type="text" [(ngModel)]="daftar.smanama"></ion-input>\n                </ion-item>\n              </div>\n            </div>\n            <!-- smatahun -->\n            <ion-item>\n              <ion-label floating>Tahun Lulus</ion-label>\n              <ion-input type="number" [(ngModel)]="daftar.smatahun"></ion-input>\n            </ion-item>\n            <!-- jurusan -->\n            <ion-item>\n              <ion-label floating>Jurusan</ion-label>\n              <ion-input type="text" [(ngModel)]="daftar.smajurusan"></ion-input>\n            </ion-item>\n            <!-- smanilai -->\n            <ion-item>\n              <ion-label floating>Rata-rata Ijazah/IPK</ion-label>\n              <ion-input type="number" [(ngModel)]="daftar.smanilai"></ion-input>\n            </ion-item>\n            \n          </div>\n          <div class="attantion">\n            Data pendaftar hanya akan diproses apabila sudah ada pembayaran di Sekretariat Pendaftaran Kampus Universitas Madura Jalan Raya Panglegur Km 3,5 Pamekasan\n          </div>\n          <br>\n          <ion-grid>\n            <ion-row>\n              <ion-col><button ion-button full (click)="btnDaftarReset()" color="color2">Reset</button></ion-col>\n              <ion-col><button ion-button full (click)="btnDaftarClicked()" color="color1">Daftar</button></ion-col>\n            </ion-row>\n          </ion-grid>\n\n        </ion-card-content>\n      </ion-card>\n    </ion-list>\n\n    <!-- content kelulusan -->      \n    <ion-list *ngSwitchCase="\'kelulusan\'">\n      <!-- <h1>{{status}}</h1> -->\n      <ion-card>\n        <ion-card-header>\n\n        </ion-card-header>\n        <ion-card-content>\n            <h2 class="text-center text-bold">INPUT KODE PENDAFTARAN</h2>\n          <ion-item>\n            <ion-label floating>Kode Pendaftaran</ion-label>\n            <ion-input type="text" [(ngModel)]="kodePengumuman"></ion-input>\n            <button style="padding: 17px 15px; font-size: 14px" ion-button item-right color="color1" (click)="getPengumuman(kodePengumuman)" ><ion-icon>Cek</ion-icon></button>\n          </ion-item>\n          <br>\n          <br>\n          <h2 class="text-center text-bold">KODE PENDAFTARAN TERSIMPAN</h2>\n          <table>\n            <thead>\n              <tr>\n                <th>Nama</th>\n                <th>Kode</th>\n                <th>Aksi</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor = "let data of kodeList">\n                <td>{{data.nama}}</td>\n                <td>{{data.kode}}</td>\n                <td style="float: right">\n                    <button ion-button color="color1" (click)="getPengumuman(data.kode)" ><ion-icon>Cek</ion-icon></button>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card *ngIf="lulus.status">\n\n        <ion-card-header>\n          <h2 class="text-center text-bold">PENGUMUMAN KELULUSAN</h2>\n        </ion-card-header>\n    \n        <ion-card-content>\n          <div>\n            <table>\n              <thead>\n                <tr>\n                  <th>Kode</th>\n                  <th>Nama</th>\n                  <th>Status</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr>\n                  <td>{{lulus.kode}}</td>\n                  <td>{{lulus.nama}}</td>\n                  <td>\n                    <span *ngIf="lulus.diterima" class="lulus">Lulus</span>\n                    <span *ngIf="!lulus.diterima" class="gagal">Tdk Lulus</span>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n            <div class="attantion">\n              Aplikasi dikembangkan sebelum pengumuman keluar, mohon melapor jika terjadi kesalahan, silahkan kunjungi website UNIRA untuk informasi yang lebih valid\n            </div>\n          </div>\n          <!-- <div *ngIf="!lulus.status">\n              <table>\n                <tbody>\n                  <tr>\n                    <td></td>\n                  </tr>\n                  <tr>\n                    <td><h2 class="text-center text-bold">BELUM ADA PENGUMUMAN</h2></td>\n                  </tr>\n                </tbody>\n              </table>\n              <br>\n          </div> -->\n        </ion-card-content>\n        \n      </ion-card>\n    </ion-list>\n\n  </div>\n</ion-content>'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_api_unira_api_unira__["a" /* ApiUniraProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BeasiswaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BeasiswaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BeasiswaPage = (function () {
    function BeasiswaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BeasiswaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BeasiswaPage');
    };
    BeasiswaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-beasiswa',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\beasiswa\beasiswa.html"*/'<ion-header>\n\n  <ion-navbar color="color1">\n    <ion-title>Beasiswa</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n\n    <ion-card-header>\n      <h2 class="text-center text-bold">BEASISWA</h2>\n    </ion-card-header>\n\n    <ion-card-content>\n      <table>\n        <thead>\n          <tr>\n            <th>No</th>\n            <th>Kategori</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>1</td>\n            <td><div>BEASISWA PPA</div></td>\n          </tr>\n          <tr>\n            <td>2</td>\n            <td><div>BEASISWA BIDIKMISI</div></td>\n          </tr>\n          <tr>\n            <td>3</td>\n            <td><div>BEASISWA BANK JATIM</div></td>\n          </tr>\n          <tr>\n            <td>4</td>\n            <td><div>BEASISWA PT DUA PUTRI KEDATON</div></td>\n          </tr>\n          <tr>\n            <td>5</td>\n            <td><div>BEASISWA DARI YAYASAN</div></td>\n          </tr>\n          <tr>\n            <td>6</td>\n            <td><div>BEASISWA PEMKAB DI MADURA</div></td>\n          </tr>\n        </tbody>\n      </table>\n    </ion-card-content>\n    \n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\beasiswa\beasiswa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], BeasiswaPage);
    return BeasiswaPage;
}());

//# sourceMappingURL=beasiswa.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BiayaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BiayaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BiayaPage = (function () {
    function BiayaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BiayaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BiayaPage');
    };
    BiayaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-biaya',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\biaya\biaya.html"*/'<ion-header>\n\n  <ion-navbar color="color1">\n    <ion-title>biaya</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card>\n\n      <ion-card-header>\n        <h2 class="text-center text-bold">BIAYA KULIAH</h2>\n      </ion-card-header>\n\n      <ion-card-content>\n          <table>\n            <thead>\n              <tr>\n                <th>Kategori</th>\n                <th>Komponen Biaya</th>\n                <th>Jumlah</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr>\n                <td>1</td>\n                <td><div>Mahasiswa Baru (Layanan Kesehatan, Pengenalan Kampus, Asuransi, dll)</div></td>\n                <td>540.000</td>\n              </tr>\n              <tr>\n                <td>2</td>\n                <td>DPP / SPP Perbulan</td>\n                <td>200.000</td>\n              </tr>\n              <tr>\n                <td>3</td>\n                <td><div>Uang Gedung / Pembangunan (Dicicil 6x dalam 1 tahun)</div></td>\n                <td></td>\n              </tr>\n              <tr>\n                <td></td>\n                <td><div>— Gelombang 1</div></td>\n                <td>2.000.000</td>\n              </tr>\n              <tr>\n                <td></td>\n                <td><div>— Gelombang 2</div></td>\n                <td>2.500.000</td>\n              </tr>\n              <tr>\n                <td></td>\n                <td><div>— Gelombang 3</div></td>\n                <td>3.000.000</td>\n              </tr>\n              <tr>\n                <td>4</td>\n                <td><div>	Heregistrasi per Semester</div></td>\n                <td>25.000</td>\n              </tr>\n              <tr>\n                <td>5</td>\n                <td><div>UAS per mata kuliah</div></td>\n                <td>15.000</td>\n              </tr>\n              <tr>\n                <td></td>\n                <td><div>Alih kredit per SKS (Khusus Mahasiswa Transfer)</div></td>\n                <td>15.000</td>\n              </tr>\n            </tbody>\n          </table>\n      </ion-card-content>\n      \n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\biaya\biaya.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], BiayaPage);
    return BiayaPage;
}());

//# sourceMappingURL=biaya.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FakultasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FakultasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FakultasPage = (function () {
    function FakultasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FakultasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FakultasPage');
    };
    FakultasPage.prototype.visitProdi = function (prodi) {
        var url = '';
        switch (prodi) {
            case 'fh':
                url = 'http://fh.unira.ac.id/';
                break;
            case 'fia':
                url = 'http://fia.unira.ac.id/';
                break;
            case 'fkip':
                url = 'http://fkip.unira.ac.id/';
                break;
            case 'fp':
                url = 'http://fp.unira.ac.id/';
                break;
            case 'ft':
                url = 'http://ft.unira.ac.id/';
                break;
            case 'fe':
                url = 'http://fe.unira.ac.id/';
                break;
        }
        console.log(url);
        window.open(url, '_system', 'location=no');
    };
    FakultasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fakultas',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\fakultas\fakultas.html"*/'\n<ion-header>\n\n  <ion-navbar color="color1">\n    <ion-title>Fakultas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <!-- fakultas hukum -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/fh.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Hukum</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Ilmu Hukum</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'fh\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- fakultas ekonomi -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/fe.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Ekonomi</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Manajemen</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-header">\n            <h2 class="text-bold">Program Studi Akuntansi</h2>\n          </div>\n          <div class="prodi-content">\n              <p>Akreditasi <b color="color1">B</b></p>\n              <hr>\n          </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'fe\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- fakultas ilmu admnistrasi  -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/fia.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Ilmu Administrasi</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Ilmu Administrasi Negara</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'fia\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- fakultas keguruan dan ilmu pendidikan -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/fkip.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Keguruan dan Ilmu Pendidikan</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Pend. Bahasa Indonesia</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Pend. Matematika</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Pend. Bahasa Inggris</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'fkip\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- fakultas pertanian -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/fp.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Pertanian</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Peternakan</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">B</b></p>\n            <hr>\n        </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'fp\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- fakultas teknik -->\n  <ion-card>\n    <ion-card-header class="no-padding">\n      <img src="assets/imgs/ft.png" alt="">\n    </ion-card-header>\n    <ion-card-content>\n      <h1 class="space-1 text-bold text-center">Fakulktas Teknik</h1>\n      <hr>\n      <div class="prodi">\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Teknik Informatika</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">C / Re Akreditasi 2017</b></p>\n            <hr>\n        </div>\n        <div class="prodi-header">\n          <h2 class="text-bold">Program Studi Teknik Sipil</h2>\n        </div>\n        <div class="prodi-content">\n            <p>Akreditasi <b color="color1">C / Re Akreditasi 2017</b></p>\n            <hr>\n        </div>\n        <div class="prodi-footer">\n          <ion-badge item-end color="color1" (click)="visitProdi(\'ft\')">\n            Kunjungi <ion-icon name="arrow-round-forward"></ion-icon>\n          </ion-badge>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\fakultas\fakultas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FakultasPage);
    return FakultasPage;
}());

//# sourceMappingURL=fakultas.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JalurPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_unira_api_unira__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the JalurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JalurPage = (function () {
    function JalurPage(ApiUnira, toastCtrl, loadingCtrl, navCtrl, navParams) {
        this.ApiUnira = ApiUnira;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.gelombangList = [];
    }
    JalurPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad JalurPage');
        this.getPMB();
    };
    JalurPage.prototype.getPMB = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present().then(function () {
            _this.ApiUnira.get('pmb').then(function (result) {
                _this.gelombangList = result['data']['relationship']['gelombang']['data'];
                //this.gelombangList = result['data']['relationship'];
                console.log(_this.gelombangList);
                loader.dismiss();
            }).catch(function (error) {
                _this.gagalKoneksi();
                loader.dismiss();
            });
        });
    };
    JalurPage.prototype.dateName = function (tanggal) {
        var monthNames = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "July",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        ];
        var dayNames = [
            "Senin",
            "Selasa",
            "Rabu",
            "Kamis",
            "Jumat",
            "Sabtu",
            "Mingu"
        ];
        //let today = new Date( yyyy, mm-1, dd );
        var today = new Date(tanggal);
        var dd = today.getDate();
        var dayname = dayNames[today.getDay() - 1];
        var mm = monthNames[today.getMonth()];
        var yyyy = today.getFullYear();
        var fullDate = dayname + ", " + dd + " " + mm + " " + yyyy;
        return fullDate;
        //alert( "the current date is: " + fullDate );
    };
    JalurPage.prototype.gagalKoneksi = function () {
        var toast = this.toastCtrl.create({
            message: 'Koneksi gagal',
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    JalurPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-jalur',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\jalur\jalur.html"*/'<ion-header>\n\n  <ion-navbar color="color1">\n    <ion-title>Jalur</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n\n  <ion-card>\n    <ion-card-header>\n      <h2 class="text-center text-bold">WAKTU PENDAFTARAN</h2>\n    </ion-card-header>\n    <ion-card-content>\n      <table *ngFor="let data of gelombangList; let i=index">\n        <thead>\n          <tr>\n            <td colspan="2" class="text-center text-bold">Gelombang {{i + 1}}</td>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>Pendaftaran</td>\n            <td><div>{{dateName(data.attributes.mulai)}} — {{dateName(data.attributes.selesai)}} </div></td>\n          </tr>\n          <tr>\n            <td>Biaya</td>\n            <td>Rp. {{data.attributes.biaya}}</td>\n          </tr>\n          <tr>\n            <td>Ujian</td>\n            <td><div>{{dateName(data.attributes.ujian)}}</div></td>\n          </tr>\n        </tbody>\n      </table>\n    </ion-card-content>\n  \n  </ion-card>\n\n    <ion-card>\n      <ion-card-header>\n        <h2 class="text-center text-bold">JALUR MASUK</h2>\n      </ion-card-header>\n      <ion-card-content>\n        \n        <ion-list>\n          <ion-item-group>\n            <ion-item-divider color="light" text-center>Jalur Tes</ion-item-divider>\n            <div>\n                Jalur Tes adalah seleksi penerimaan mahasiswa baru yang diselenggarakan secara mandiri oleh Universitas Madura. Seleksi dengan jalur ini merupakan pola seleksi berdasarkan hasil tes tertulis dan praktik komputer untuk Jurusan Teknik Informatika.\n            </div>\n          </ion-item-group>\n        </ion-list>  \n\n        <ion-list class="space-2">\n            <ion-item-group>\n              <ion-item-divider color="light" text-center>Jalur Prestasi</ion-item-divider>\n              <div>Jalur Prestasi adalah fasilitas bagi calon mahasiswa baru yaitu:</div>\n              <div class="jalur_prestasi">\n                <h2 class=" text-bold"><ion-icon name="md-checkmark" color="secondary"></ion-icon> Bebas Biaya Bangunan</h2>\n                <div class="left-1">1. Bagi Rangking 1-3 Tingkat Sekolah</div>\n                <div class="left-1">2. Bagi Rangking 1-3 Tingkat Kelas jika Mendaftar ke FKIP</div>\n                <div class="left-1">3. uara 1-3 Lomba yang diadakan Prodi di Universitas Madura jika mendaftar ke Prodi Penyelenggara Lomba</div>\n              </div>\n              <div class="jalur_prestasi">\n                <h2 class=" text-bold"><ion-icon name="md-checkmark" color="secondary"></ion-icon> Bebas DPP/SPP 1 Semester</h2>\n                <div class="left-1">Jika Juara 1 Lomba Tingkat Kabupaten (mewakili sekolah)</div>\n              </div>\n            </ion-item-group>\n          </ion-list> \n\n      </ion-card-content>\n    \n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\jalur\jalur.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_unira_api_unira__["a" /* ApiUniraProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], JalurPage);
    return JalurPage;
}());

//# sourceMappingURL=jalur.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchKabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_unira_api_unira__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchKabPage = (function () {
    function SearchKabPage(ApiUnira, events, navCtrl, navParams) {
        this.ApiUnira = ApiUnira;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.key = '';
        this.name = '';
        this.title = '';
        this.mode = '';
        this.loding = false;
        this.kodeWilayah = '';
        this.bentuk = '';
        this.mode = this.navParams.get('mode');
        console.log(this.mode);
        if (this.mode == 'lahir kota' || this.mode == 'asal kota' || this.mode == 'sma kota') {
            // this.mode != 'lahir' ?  param = { 'q' : q} :null;
            this.title = this.toTitleCase(this.mode);
            this.getKabupaten();
        }
        else {
            this.title = 'Sekolah Kota';
            this.kodeWilayah = this.navParams.get('kode_wilayah');
            this.bentuk = this.navParams.get('bentuk');
            this.getsekolah();
        }
    }
    SearchKabPage.prototype.ionViewDidLoad = function () {
    };
    SearchKabPage.prototype.getItems = function (searchbar) {
        var q = searchbar.srcElement.value;
        if (!q) {
            return;
        }
        if (this.mode == 'lahir kota' || this.mode == 'asal kota' || this.mode == 'sma kota') {
            this.getKabupaten(q);
        }
        else {
            this.getsekolah(q);
        }
    };
    SearchKabPage.prototype.listKabClicked = function (key, name) {
        this.events.publish('search:created', key, name);
        this.navCtrl.remove(1);
    };
    SearchKabPage.prototype.getKabupaten = function (q) {
        var _this = this;
        if (q === void 0) { q = null; }
        this.loding = true;
        var param = {};
        q != null ? param = { 'q': String(q) } : null;
        this.ApiUnira.get('wilayah', param).then(function (result) {
            _this.kabupatenList = result;
            _this.loding = false;
        }).catch(function (error) {
        });
    };
    SearchKabPage.prototype.getsekolah = function (q) {
        var _this = this;
        if (q === void 0) { q = null; }
        this.loding = true;
        var param = {};
        param = { 'filter[wilayah]': this.kodeWilayah };
        if (q != null) {
            param = { 'filter[wilayah]': String(this.kodeWilayah), 'filter[nama]': String(q) };
        }
        this.ApiUnira.get(this.bentuk, param).then(function (result) {
            _this.sekolahList = result['data'];
            _this.loding = false;
        }).catch(function (error) {
        });
    };
    SearchKabPage.prototype.toTitleCase = function (str) {
        return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
    };
    SearchKabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-kab',template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\pages\search-kab\search-kab.html"*/'<ion-header>\n\n  <ion-navbar color="color1">\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n  <ion-searchbar color="color1" (ionInput)="getItems($event)"></ion-searchbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item-group>\n      <ion-item *ngIf="loding">Loading...</ion-item>\n      <div *ngIf="mode != \'sma nama\'">\n        <ion-item (click)="listKabClicked(data.id, data.text)" *ngFor="let data of kabupatenList">{{data.text}}</ion-item>\n      </div>\n      <div *ngIf="mode == \'sma nama\'">\n        <ion-item (click)="listKabClicked(data.id, data.attributes.nama)" *ngFor="let data of sekolahList">{{data.attributes.nama}}</ion-item>\n      </div>\n    </ion-item-group>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\pages\search-kab\search-kab.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_unira_api_unira__["a" /* ApiUniraProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SearchKabPage);
    return SearchKabPage;
}());

//# sourceMappingURL=search-kab.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(230);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_beasiswa_beasiswa__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_biaya_biaya__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_fakultas_fakultas__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_jalur_jalur__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_search_kab_search_kab__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_api_unira_api_unira__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_helper_helper__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_date_picker__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_geolocation__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_location_accuracy__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_beasiswa_beasiswa__["a" /* BeasiswaPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_biaya_biaya__["a" /* BiayaPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_fakultas_fakultas__["a" /* FakultasPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_jalur_jalur__["a" /* JalurPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_search_kab_search_kab__["a" /* SearchKabPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_beasiswa_beasiswa__["a" /* BeasiswaPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_biaya_biaya__["a" /* BiayaPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_fakultas_fakultas__["a" /* FakultasPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_jalur_jalur__["a" /* JalurPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_search_kab_search_kab__["a" /* SearchKabPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_api_unira_api_unira__["a" /* ApiUniraProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_helper_helper__["a" /* HelperProvider */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_location_accuracy__["a" /* LocationAccuracy */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"X:\Data\Project\Ionic\pmbUnira\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"X:\Data\Project\Ionic\pmbUnira\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelperProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the HelperProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HelperProvider = (function () {
    function HelperProvider(http) {
        this.http = http;
        console.log('Hello HelperProvider Provider');
    }
    HelperProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], HelperProvider);
    return HelperProvider;
}());

//# sourceMappingURL=helper.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiUniraProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ApiUniraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ApiUniraProvider = (function () {
    function ApiUniraProvider(http) {
        this.http = http;
        this.endPoint = {
            'krs': '/v1/krs',
            'khs': '/v1/khs',
            'saya': '/v1/saya',
            'token': '/v1/token',
            'dokar': '/v1/dokar',
            'thajaran': '/v1/thajaran',
            'sksmaks': '/v1/sks/maksimum',
            'prodi': '/v1/prodi',
            'wilayah': '/wilayah',
            'smk': '/v1/sekolah/smk',
            'sma': '/v1/sekolah/sma',
            'ma': '/v1/sekolah/ma',
            'pmbpendaftar': '/v1/pmb/pendaftar',
            'pmb': '/v1/pmb'
        };
        this.apiUrl = 'http://api1.unira.ac.id';
        console.log('Hello ApiUniraProvider');
    }
    ApiUniraProvider.prototype.sapa = function () {
        return new Promise(function (resolve, reject) {
            resolve('hai');
        });
    };
    ApiUniraProvider.prototype.get = function (type, param, header) {
        var _this = this;
        if (param === void 0) { param = null; }
        if (header === void 0) { header = null; }
        return new Promise(function (resolve, reject) {
            var headers = _this.header(header);
            var params = _this.param(param);
            _this.http.get(_this.apiUrl + _this.endPoint[type], { params: params, headers: headers }).subscribe(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    ApiUniraProvider.prototype.post = function (type, param, header) {
        var _this = this;
        if (param === void 0) { param = null; }
        if (header === void 0) { header = null; }
        var headers = this.header(header);
        var params = this.param(param);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + _this.endPoint[type], params, headers).subscribe(function (data) {
                resolve(data);
            }, function (error) {
                console.log(error);
                reject(error);
            });
        });
    };
    ApiUniraProvider.prototype.header = function (header) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        if (header != null) {
            Object.keys(header).forEach(function (key) {
                headers = headers.set(key, header[key]);
            });
            return headers;
        }
        return {};
    };
    ApiUniraProvider.prototype.param = function (param) {
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]();
        if (param != null) {
            Object.keys(param).forEach(function (key) {
                params = params.append(key, param[key]);
            });
            return params;
        }
        return {};
    };
    ApiUniraProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ApiUniraProvider);
    return ApiUniraProvider;
}());

//# sourceMappingURL=api-unira.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map