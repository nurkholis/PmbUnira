import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BeasiswaPage } from '../pages/beasiswa/beasiswa';
import { BiayaPage } from '../pages/biaya/biaya';
import { FakultasPage } from '../pages/fakultas/fakultas';
import { JalurPage } from '../pages/jalur/jalur';
import { SearchKabPage } from '../pages/search-kab/search-kab';

import { HttpClientModule } from '@angular/common/http';
import { ApiUniraProvider } from '../providers/api-unira/api-unira';
import { HelperProvider } from '../providers/helper/helper';
import { DatePicker } from '@ionic-native/date-picker';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BeasiswaPage,
    BiayaPage,
    FakultasPage,
    JalurPage,
    SearchKabPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BeasiswaPage,
    BiayaPage,
    FakultasPage,
    JalurPage,
    SearchKabPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiUniraProvider,
    HelperProvider,
    DatePicker,
    Geolocation,
    LocationAccuracy
  ]
})
export class AppModule {}
