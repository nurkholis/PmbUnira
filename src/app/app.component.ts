import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { BeasiswaPage } from '../pages/beasiswa/beasiswa';
import { BiayaPage } from '../pages/biaya/biaya';
import { FakultasPage } from '../pages/fakultas/fakultas';
import { JalurPage } from '../pages/jalur/jalur';
import { SearchKabPage } from '../pages/search-kab/search-kab';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

