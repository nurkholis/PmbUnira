import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FakultasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-fakultas',
  templateUrl: 'fakultas.html',
})
export class FakultasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FakultasPage');
  }

  visitProdi(prodi:String){
    let url:string= '';
    switch(prodi){
      case 'fh' : url = 'http://fh.unira.ac.id/'; break;
      case 'fia' : url = 'http://fia.unira.ac.id/'; break;
      case 'fkip' : url = 'http://fkip.unira.ac.id/'; break;
      case 'fp' : url = 'http://fp.unira.ac.id/'; break;
      case 'ft' : url = 'http://ft.unira.ac.id/'; break;
      case 'fe' : url = 'http://fe.unira.ac.id/'; break;
    }
    console.log(url);
    window.open(url, '_system', 'location=no');
  }

}
