import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ApiUniraProvider } from '../../providers/api-unira/api-unira';

/**
 * Generated class for the JalurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-jalur',
  templateUrl: 'jalur.html',
})
export class JalurPage {

  gelombangList = [];
  constructor(
    public ApiUnira: ApiUniraProvider, 
    public toastCtrl: ToastController, 
    public loadingCtrl: LoadingController, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JalurPage');
    this.getPMB();
  }
  getPMB(){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present().then(()=>{
      this.ApiUnira.get('pmb').then((result)=>{
      this.gelombangList = result['data']['relationship']['gelombang']['data'];
        //this.gelombangList = result['data']['relationship'];
        console.log(this.gelombangList);
        
        loader.dismiss();
      }).catch((error)=>{
        this.gagalKoneksi();
        loader.dismiss();
      });
    });
  }
  dateName(tanggal) {
    let monthNames = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "July",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];

    let dayNames = [
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu",
        "Mingu"
    ];
    //let today = new Date( yyyy, mm-1, dd );
    let today = new Date(tanggal);
    let dd = today.getDate();
    let dayname = dayNames[today.getDay()-1];
    let mm = monthNames[today.getMonth()];
    let yyyy = today.getFullYear();
    let fullDate = dayname + ", " + dd + " " + mm + " " + yyyy;
    return fullDate;
    //alert( "the current date is: " + fullDate );
  }
  gagalKoneksi(){
    let toast = this.toastCtrl.create({
      message: 'Koneksi gagal',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
