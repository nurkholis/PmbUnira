import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, NavParams, AlertController, Platform } from 'ionic-angular';

import { BeasiswaPage } from '../beasiswa/beasiswa';
import { BiayaPage } from '../biaya/biaya';
import { FakultasPage } from '../fakultas/fakultas';
import { JalurPage } from '../jalur/jalur';
import { SearchKabPage } from '../search-kab/search-kab';

import { ApiUniraProvider } from '../../providers/api-unira/api-unira';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { Events } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  latitude:any;
  longitude:any;
  pmb: string = "beranda";
  kode_wilayah = '';
  prodiList = [];
  kabupatenList:any;
  sekolahList = [];
  kodeList = [];
  kodePengumuman = '';
  status:any = 'defaut';
  daftar = {
    'prodi'  : '22',
    'prodi2'  : '21',
    'masuk'   : '1',
    'kelas'   : '1',
    'nama'    : 'Dummy',
    'agama'   : '1',
    'ktp'     : '1234567890',
    'jeniskelamin'  :'L',
    'lahirkota' : '',
    'lahirkotanama' : '',
    'lahirtanggal'  : '20/07/1996',
    'alamat'  : '',
    'kota'    : '',
    'kotanama'    : '',
    'kodepos' : '',
    'telepon' : '085233889578',
    'email'   : 'coab@coba.coba',
    'smakota' : '',
    'smakotanama' : '',
    'smanama' : '',
    'smatahun': '2016',
    'smajurusan': 'IPS',
    'smanilai': '90',
    'bentuksekolah': '',
    'time': ''
  }
  lulus = {
    nama  : '',
    kode  : '',
    status: false,
    diterima: null,
  }
  loadstatus = false;
  
  constructor(
    public platform: Platform,
    public locationAccuracy: LocationAccuracy,
    public alertCtrl: AlertController,
    public geolocation: Geolocation,
    public datePicker: DatePicker,
    public navParams: NavParams,
    public events: Events, 
    public http: HttpClient, 
    public toastCtrl: ToastController, 
    public loadingCtrl: LoadingController, 
    public ApiUnira: ApiUniraProvider, 
    public navCtrl: NavController) {
      this.closesHandle();
      //localStorage.clear();
      // membuat data kode
      if(localStorage.getItem('kode') == null){
        console.log('membuat kode');
        let a = {
          'data': []
        }
        localStorage.setItem('kode',JSON.stringify(a))    ;    
      }
  }

  ionViewDidLoad() {   
    
  }

  navClicked(){
    console.log(this.pmb);
    if(this.pmb == 'daftar'){
      if(this.loadstatus == false){
        this.activatedGPS();
        this.loadstatus = true;
        let loader = this.loadingCtrl.create({
          content: "Please wait..."
        });
        loader.present().then(()=>{
          //loader.dismiss();
          //mengambil data prodi
          this.getProdi().then((result)=>{
            this.prodiList = result['data'];
            loader.dismiss();
          }).catch((error)=>{
            this.gagalKoneksi();
            loader.dismiss();
          });      
        });
      }
      let now = (new Date().getTime()) / 1000;
      this.daftar.time = now.toFixed(0) ;
      console.log(this.daftar.time);
    }
    if(this.pmb == 'kelulusan'){
      //this.kelulusan();
      this.showKode();
    }
  }

  closesHandle(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        let alertClose = this.alertCtrl.create({
          title: 'Keluar dari aplikasi ?',
          buttons: [
            {
              text: 'Batal',role: 'cancel',handler: kelas => {
              }
            },
            {
              text: 'Keluar', handler: data => {
                this.platform.exitApp();
              }
            }
          ]
        });
        alertClose.present();
      });
    });
  }

  gagalKoneksi(s=null){
    if(s==null){
      let toast = this.toastCtrl.create({
        message: 'Koneksi gagal',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    else{
      let toast = this.toastCtrl.create({
        message: s,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  // kode beranda
  pushPage(page:String){
    if(page == 'fakultas'){
      this.navCtrl.push(FakultasPage);
    }if(page == 'jalur'){
      this.navCtrl.push(JalurPage);
    }if(page == 'biaya'){
      this.navCtrl.push(BiayaPage);
    }if(page == 'beasiswa'){
      this.navCtrl.push(BeasiswaPage);
    }
  }
  // kahir kode beranda

  //kode daftar
  activatedGPS(){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(()=>{
            this.loadLocation();
            this.status = 'actived xx';
          },error =>{
            this.status = 'disactived';
          }
        );
      }else{
        this.status = 'not can request';
      }
    
    });
  }
  loadLocation() {
    let options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    const watch = this.geolocation.watchPosition(options);
    watch.subscribe((data) => {
      if (data.coords !== undefined) {
        this.status = data.coords.latitude;
        this.latitude = data.coords.latitude;
        this.longitude = data.coords.longitude;
        console.log(this.latitude, this.longitude);
        // harus registes key
        this.getAddress(this.latitude, this.longitude).then((result)=>{
          this.daftar.alamat = result['results'][0]['formatted_address'];
          let data = result['results'][0]['address_components'];
          for(let i = 0; i<data.length; i++){
            if(data[i]['types'][0] == 'postal_code'){
              console.log(data[i]['long_name']);
              this.daftar.kodepos = data[i]['long_name'];
            }
          }
        }).catch((reject)=>{
          console.log('gagal memuat alamat');
        })
      } else {         
        console.log('Error lokasi');
        this.status = 'Error lokasi';
      }
    });
  }
  getDate(){
    console.log('getdate');
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
    }).then((date)=>{
      console.log('Got date: ', date);
      let today:any = date;
      let dd:any = today.getDate();
      let mm:any = today.getMonth()+1; //January is 0!

      let yyyy:any = today.getFullYear();
      if(dd<10){
          dd='0'+dd;
      } 
      if(mm<10){
          mm='0'+mm;
      } 
      let jadi = dd+'/'+mm+'/'+yyyy;
      this.daftar.lahirtanggal = jadi;
    },(err)=>{
      console.log('Error occurred while getting date: ', err);
      this.status = err;
    }
      // date => console.log('Got date: ', date),
      // err => console.log('Error occurred while getting date: ', err)
    );
  }
  getProdi(){
    return new Promise((resolve, reject)=>{
      this.ApiUnira.get('prodi').then((result)=>{
        resolve(result);
      }).catch((error)=>{
        reject(error)
      });
    });
  }
  getAddress(lat:string, long:string){
    return new Promise((resolve, reject)=>{
      this.http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat + ',' + long).subscribe(data => {
        resolve(data);
        console.log('get addr');
        
      }, error => {
        reject(error);
      });
    })
  }
  getKabupaten(){
    return new Promise((resolve, reject)=>{
      this.ApiUnira.get('wilayah', {'limit':'1000'}).then((result)=>{
        resolve(result);
      }).catch((error)=>{
        reject(error)
      });
    });
  }
  search(to){
    if(to == 'asal kota' || to == 'lahir kota' || to == 'sma kota'){
      this.navCtrl.push(SearchKabPage, {'mode' : to});

      this.events.subscribe('search:created', (key, name) => {
        if(to == 'asal kota' ){
          this.daftar.kota = key; this.daftar.kotanama =name;
        }
        if(to == 'lahir kota'){
          this.daftar.lahirkota = key; this.daftar.lahirkotanama=name;
        }
        if(to == 'sma kota'){
          this.daftar.smakota = key; this.daftar.smakotanama=name;
        }
        this.events.unsubscribe('search:created', null);
      });
    }
    if(to == 'sma nama'){
      let param = {
        'mode' : to,
        'bentuk': this.daftar.bentuksekolah,
        'kode_wilayah': this.daftar.smakota
      }
      this.navCtrl.push(SearchKabPage, param);
      this.events.subscribe('search:created', (key, name) => {
        this.daftar.smanama = name;
        this.events.unsubscribe('search:created', null);
      });
    }
  }
  bentuksekolahChanged(){
    this.daftar.smanama = '';
  }
  smaKotaChanged(){
    this.daftar.bentuksekolah = '';
    this.daftar.smanama = '';
  }
  pmbDaftar(){
    return new Promise((resolve, reject)=>{
      this.ApiUnira.post('pmbpendaftar', this.daftar).then((result)=>{
        resolve(result);
      }).catch((error)=>{
        reject(error)
      });
    });
  }
  btnDaftarClicked(){
    console.log(this.daftar);
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present().then(()=>{

      this.pmbDaftar().then((result)=>{
        let nomor = result['data']['attributes']['nomor'];
        let nama = result['data']['attributes']['nama'];
        let alert = this.alertCtrl.create({
          title: 'Berhasil Mendaftar',
          subTitle: 'Anda terdaftar dengan Kode ' + nomor + ' dan Nama ' + nama + ". Bukti pendaftaran sudah dikirim ke e-mail anda",
          buttons: ['Ok']
        });
        loader.dismiss();
        alert.present();
        //menambah data kode
        let b = localStorage.getItem('kode');
        b = JSON.parse(b);
        b['data'].push({'nama':nama,'kode':nomor});
        localStorage.setItem('kode',JSON.stringify(b));
        console.log(JSON.parse(localStorage.getItem('kode')));
        this.showKode();
        //mengirim email pada pendaftar
        this.http.get('http://api1.unira.ac.id/v1/pmb/mail/' + nomor).subscribe(data => {
        }, error => {
        });

      }).catch((error)=>{
        
        console.log(error);
        
        console.log(error['error']['errors'][0]['detail']);
        let detail = error['error']['errors'][0]['detail'];
        let alert = this.alertCtrl.create({
          title: 'Gagal Mendaftar',
          subTitle: detail,
          buttons: ['Ok']
        });
        alert.present();
        loader.dismiss();
      });

    });
    
  }
  btnDaftarReset(){
    let alert = this.alertCtrl.create({
      title: 'Reset',
      message: 'Semua data yang ada dalam form akan di hapus',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oke',
          handler: () => {
            this.resetForm();
          }
        }
      ]
    });
    alert.present()
  }
  resetForm(){
    this.daftar.prodi = '';
    this.daftar.prodi2 = '';
    this.daftar.masuk = '';
    this.daftar.kelas = '';
    this.daftar.nama = '';
    this.daftar.agama = '';
    this.daftar.ktp = '';
    this.daftar.jeniskelamin = '';
    this.daftar.lahirkota = '';
    this.daftar.lahirkotanama = '';
    this.daftar.lahirtanggal = '';
    this.daftar.alamat = '';
    this.daftar.kota = '';
    this.daftar.kotanama = '';
    this.daftar.kodepos = '';
    this.daftar.telepon = '';
    this.daftar.email = '';
    this.daftar.smakota = '';
    this.daftar.smakotanama = '';
    this.daftar.bentuksekolah = '';
    this.daftar.smanama = '';
    this.daftar.smatahun = '';
    this.daftar.smajurusan = '';
    this.daftar.smanilai = '';
  }
  //akhir kode daftar

  //kode kelulusan
  showKode(){
    let kode = JSON.parse(localStorage.getItem('kode'));
    this.kodeList = kode['data'];
    this.kodePengumuman = this.kodeList[this.kodeList.length - 1]['kode'];
  }
  getPengumuman(a){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present().then(()=>{
      this.http.get('http://api1.unira.ac.id/v1/pmb/pendaftar/'+ a +'?field=nama,diterima').subscribe(data => {
        this.lulus.kode = data['data']['id'];
        this.lulus.nama = data['data']['attributes']['nama'];
        this.lulus.diterima = data['data']['attributes']['diterima'];
        if(this.lulus.diterima != null){
          this.lulus.status = true;
        }else{
          let alert = this.alertCtrl.create({
            title: 'Belum ada pengumuman !',
            subTitle: 'Untuk informasi lebih jelas silahkan kunjungi website UNIRA atau hubungi admin PMB',
            buttons: ['Ok']
          });
          alert.present();
        }
        console.log(this.lulus);
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.gagalKoneksi();
      });

    });
  }
  
  //akhir kode kelulusan

}
