import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Events } from 'ionic-angular';
import { ApiUniraProvider } from '../../providers/api-unira/api-unira';

@Component({
  selector: 'page-search-kab',
  templateUrl: 'search-kab.html',
})
export class SearchKabPage {

  kabupatenList:any;
  sekolahList:any;
  key = '';
  name = '';
  title = '';
  mode = '';
  loding = false;
  kodeWilayah = '';
  bentuk = '';


  constructor(
    public ApiUnira: ApiUniraProvider, 
    public events: Events, 
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.mode = this.navParams.get('mode');
    console.log(this.mode);

    if(this.mode == 'lahir kota' || this.mode == 'asal kota' || this.mode == 'sma kota'){
      // this.mode != 'lahir' ?  param = { 'q' : q} :null;
      this.title = this.toTitleCase(this.mode);
      this.getKabupaten();
    }else{
      this.title = 'Sekolah Kota';
      this.kodeWilayah = this.navParams.get('kode_wilayah');
      this.bentuk = this.navParams.get('bentuk');
      this.getsekolah();
    }
  }

  ionViewDidLoad() {
    
  }

  getItems(searchbar){
    var q = searchbar.srcElement.value;
    if (!q) {
      return;
    }
    if(this.mode == 'lahir kota' || this.mode == 'asal kota' || this.mode == 'sma kota'){
      this.getKabupaten(q);
    }else{
      this.getsekolah(q);
    }      
  }

  listKabClicked(key, name){
    this.events.publish('search:created', key, name);
    this.navCtrl.remove(1);
  }

  getKabupaten(q = null){
    this.loding = true;
    let param = {};
    q != null ?  param = { 'q' : String(q)} :null;
    this.ApiUnira.get('wilayah', param).then((result)=>{
      this.kabupatenList = result;
      this.loding = false;
    }).catch((error)=>{
      
    });
  }

  getsekolah(q = null){
    this.loding = true;
    let param = {};
    param = {'filter[wilayah]': this.kodeWilayah};
    if(q != null ){
      param = {'filter[wilayah]': String(this.kodeWilayah), 'filter[nama]': String(q)};
    }
    this.ApiUnira.get(this.bentuk, param).then((result)=>{
      this.sekolahList = result['data'];
      this.loding = false;
    }).catch((error)=>{
      
    });
  }

  toTitleCase(str)
  {
      return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }

}
