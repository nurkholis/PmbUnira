import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';

/*
  Generated class for the ApiUniraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiUniraProvider {
  endPoint = {
    'krs'     : '/v1/krs',
    'khs'     : '/v1/khs',
    'saya'    : '/v1/saya',
    'token'   : '/v1/token',
    'dokar'   : '/v1/dokar',
    'thajaran': '/v1/thajaran',
    'sksmaks' : '/v1/sks/maksimum',
    'prodi'   : '/v1/prodi',
    'wilayah' : '/wilayah',
    'smk'     : '/v1/sekolah/smk',
    'sma'     : '/v1/sekolah/sma',
    'ma'      : '/v1/sekolah/ma',
    'pmbpendaftar' : '/v1/pmb/pendaftar',
    'pmb'     : '/v1/pmb'
  };
  apiUrl = 'http://api1.unira.ac.id'
  constructor(public http: HttpClient) {
    console.log('Hello ApiUniraProvider');
  }

  public sapa(){
    return new Promise( (resolve, reject) =>{
      resolve('hai');
    });
    
  }

  public get(type:any, param:Object = null, header:Object = null){
    return new Promise( (resolve, reject) =>{

      let headers = this.header(header);
      let params = this.param(param);

      this.http.get(this.apiUrl+this.endPoint[type], {params: params , headers: headers}).subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      });
    });
  }

  public post(type:any, param:Object = null , header:Object = null){
    let headers = this.header(header);
    let params = this.param(param);
    
    return new Promise( (resolve,  reject)=>{
      this.http.post(this.apiUrl+this.endPoint[type], params, headers).subscribe(data =>{
        resolve(data);        
      },error=>{
        console.log(error);
        reject(error);
      });
    });

  }

  public header(header:Object){
    let headers: HttpHeaders = new HttpHeaders();
    if (header != null){
      Object.keys(header).forEach(function(key) {
        headers = headers.set(key, header[key]);
      });
      return headers;
    }
    return {};
  }

  public param(param:object){
    let params: HttpParams = new HttpParams();
    if (param != null){
      Object.keys(param).forEach(function(key) {
        params = params.append(key, param[key]);
      });
      return params;
    }
    return {};
  }

}
